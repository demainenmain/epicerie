import React from 'react'
import { Link } from 'gatsby'

import logo from '../img/logo.svg'
import facebook from '../img/social/facebook.svg'
import gitlab from '../img/gitlab-icon.svg'

const Footer = class extends React.Component {
  render() {
    return (
      <footer className="footer has-background-black has-text-white-ter">
        <div className="content has-text-centered">
          <img
            src={logo}
            alt="Demain en Main - Épicerie Paysanne"
            style={{ width: '14em', height: '10em', backgroundColor: '#fff', borderRadius: '60% 60% 10% 10%' }}
          />
        </div>
        <div className="content has-text-centered has-background-black has-text-white-ter">
          <div className="container has-background-black has-text-white-ter">
            <div style={{ maxWidth: '100vw' }} className="columns">
              <div className="column is-4">
                <section className="menu">
                  <ul className="menu-list">
                    <li>
                      <Link to="/" className="navbar-item">
                        Accueil
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/about">
                        Qui sommes nous ?
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/products">
                        Produits
                      </Link>
                    </li>
                    <li>
                      <a
                        className="navbar-item"
                        href="/admin/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Admin
                      </a>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4">
                <section>
                  <ul className="menu-list">
                    <li>
                      <Link className="navbar-item" to="/blog">
                        Dernière nouvelles
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/contact">
                        Nous contacter
                      </Link>
                    </li>
                    <li>
                      <a
                        rel="noopener noreferrer"
                        className="navbar-item"
                        title="Faire un don via HelloAsso"
                        href="https://www.donnerenligne.fr/coclicco/faire-un-don">
                        Faire un don
                      </a>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4 social">
                <a title="facebook" href="https://www.facebook.com/Demain-En-Main-Epicerie-Paysanne-116041389800399/">
                  <img
                    src={facebook}
                    alt="Facebook"
                    style={{ width: '1em', height: '1em' }}
                  />
                </a>
                <a title="gitlab" href="https://gitlab.com/demainenmain/epicerie"
                   target="_blank"
                   rel="noopener noreferrer">
                  <img
                    src={gitlab}
                    alt="Gitlab"
                    style={{ width: '1em', height: '1em' }}
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
