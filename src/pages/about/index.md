---
templateKey: about-page
title: Notre Charte
---
**Souhaitant un monde meilleur, nous avons rédigé une charte « éthique » que chaque producteur du groupement s’engage à respecter. Elle traite de la qualité des produits, mais aussi de celle des cultures, des élevages, des modes de transformation comme des relations humaines.**

### La voici:

La présente Charte exprime les **valeurs collectives partagées** par les producteurs et les consommateurs de Demain en Main qui définissent les **qualités sociales, environnementales et économiques des produits que nous souhaitons produire, rendre accessibles et consommer**. 

Elle est mise en œuvre par un règlement intérieur et par nos pratiques quotidiennes.

Nous, producteurs de Demain en Main, nous engageons à fournir des produits de qualité en se basant sur les points suivants :



Nos **élevages** sont de plein air et nous respectons la vie de nos animaux. Nos animaux sont prioritairement nourris par pâturage ainsi qu’avec les fourrages et les céréales produits sur l’exploitation ou achetés localement.

**Nous nous interdisons** : les aliments génétiquement modifiés ou médicamenteux, les farines animales, les aliments fermentés, les cultures hors sol, les élevages concentrationnaires 

Nos **cultures** sont de saison et adaptées à leur environnement. Elles sont conduites dans un cycle de rotation équilibré. 

**Nous nous interdisons** : tout produit phytosanitaire interdit en agriculture biologique, OGM, serres chauffées 

Nos **transformations** n’utilisent que des assaisonnements naturels, des levures, des levains,des ferments lactiques et de la présure, et privilégient le temps d’affinage.

**Nous nous interdisons** : colorants et conservateurs de synthèse, OGM 



Nos **fournisseurs de matières premières** sont locaux et respectent la même éthique que nous. Nous ne commercialisons que les produits issus de notre propre travail, sauf pour satisfaire une diversité minimum d’approvisionnement. Dans ce cas, les produits font l’objet d’un signalement explicite. Nous vendons en priorité en circuit court et au moins localement. Nous garantissons ainsi la qualité de nos productions et de leur distribution au consommateur 



Nous nous engageons à présenter nos **entreprises** à l’association et au public. Si nous ne sommes pas certifiés en AB ou Nature&Progrès, nous soumettrons nos pratiques au contrôle participatif de l’association. 

Nous invitons cordialement toute personne intéressée à venir visiter nos fermes, dans la limite et le respect de l’exercice de nos métiers. Ainsi nous garantissons une démarche de totale transparence 



Nous souhaitons **partager** une solidarité morale et active face aux difficultés que chacun d’entre nous peut rencontrer dans l’exercice de son activité.

Nous nous engageons à partager les discussions et les décisions de l’association avec lesproducteurs et les consommateurs. Finalement nous souhaitons aussi faire de notre associationun espace de rencontre et de convivialité entre tous.



**Les Producteurs de Demain en Main**
