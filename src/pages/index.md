---
templateKey: index-page
title: ' ㅤ ㅤ ㅤ ㅤㅤ ㅤ ㅤ ㅤ ㅤ'
image: /img/banniere.jpeg
heading: ' ㅤ ㅤ ㅤ ㅤㅤ ㅤ ㅤ ㅤ ㅤ'
subheading: ' ㅤ ㅤ ㅤ ㅤㅤ ㅤ ㅤ ㅤ ㅤ'
mainpitch:
  title: Une épicerie participative en circuit court
  description: >-
    Producteurs et consommateurs sont les acteurs de ce lieu. Sans
    intermédiaires, les produits passent de main en main.
description: '    Envie de manger local, bon, beau? et de participer à une aventure humaine? venez nous rendre visite ou commandez directement sur https://app.cagette.net/home'
intro:
  blurbs:
    - image: /img/cagette.net.jpg
      text: >-


        Commencez par aller vous inscrire gratuitement sur le site de vente du
        groupement: cagette.net. C’est simple, un e-mail, un contact (pour les
        oublis), un mot de passe et c’est parti!
    - image: /img/calendrier.jpg
      text: >


        Choisissez la date de livraison de votre panier parmi celles proposées
        sur votre page d’accueil et cliquer. Un e-mail 24h avant vous rappellera
        d’aller chercher votre panier. Vous avez à partir de huit jours et
        jusqu’à 48h avant la livraison pour commander vos produits.
    - image: /img/fotoarok-1024x1024-300x200.png
      text: >
        Choisissez vos produits parmi tous les produits frais, locaux et de
        saison, proposés par l’ensemble des producteurs.  N’oubliez pas de
        valider votre commande à la fin de vos choix. Et prenez un moment pour
        lire notre charte de qualité en cliquant sur ...
    - image: /img/carte.png
      text: >
        Venez récupérer vos produits au 3 rue du général de Gaulle, à l’épicerie
        paysanne Demain en Main, le vendredi entre 16h30 et 19h30 ou le samedi
        entre 10h et 13h récupérer votre panier avec tous vos produits en une
        seule fois (amenez vos sacs, cabas ou paniers). Vous pouvez payer par
        chèque ou espèces d’un seul coup l’ensemble sur place.
  heading: Nos produits
  description: |
    ...
main:
  heading: a
  description: |
    b
  image1:
    alt: 'A '
    image: /img/Demain en main.png
  image2:
    alt: c
    image: /img/fotoarok-1024x1024-300x200.png
  image3:
    alt: d
    image: /img/carte.png
---

