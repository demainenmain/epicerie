---
templateKey: blog-post
title: MODE DRIVE !
date: 2020-04-14T10:20:01.141Z
description: >-
  Dans le contexte actuel, l'équipe de Demain en Main vous propose de récupérer
  vos paniers commandés sur Cagette.com devant la boutique, aux horaires
  d'ouverture habituels les vendredi soirs et samedi matins, sans devoir sortir
  de votre voiture. 


  L'épicerie elle-même reste fermée au public jusqu'à nouvel ordre. 
featuredpost: false
featuredimage: /img/images-3.jpg
tags:
  - Mode Drive
---
![](/img/chemex.jpg)
