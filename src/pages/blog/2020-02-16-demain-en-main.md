---
templateKey: blog-post
title: DEMAIN EN MAIN
date: 2020-02-16T17:12:14.676Z
description: >-
  Epicerie participative en circuit court  située au cœur des Monts d'Arrée à
  Huelgoat
featuredpost: true
featuredimage: /img/Demain en main.png
tags:
  - épicerie
  - magasin de producteurs
  - circuit court
  - associatif
  - participatif
  - huelgoat
  - finistère
  - cocclicot
---
###### Cette épicerie...

 ...n'est pas comme les autres: elle nous appartient à tous, à nous de la faire vivre!

​

Que nous soyons producteurs ou consommateurs, nous pouvons devenir les acteurs de ce lieu afin qu'il soit non seulement utile, mais aussi convivial et créatif.

 Ainsi nous prenons tous ensemble "demain en main"...
