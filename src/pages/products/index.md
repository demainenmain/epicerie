---
templateKey: product-page
title: Nos producteurs et nos produits
image: /img/fotoarok-1024x1024-300x200.png
heading: >-
  Actifs, déposants ou en achat/reventes....nous, les producteurs, faisont vivre
  le magasin...venez nous rencontrer! ...et participer si le coeur vous en dit
description: >-
  C'est fou tout ce qu'on trouve à Demain en Main! N’hésitez plus, venez voir et
  goûter sur place! Et si vous êtes producteur et interessé, que vous respectez
  la charte éthique et qu’il n’y a pas déjà des produits similaires proposés au
  magasin, contactez nous.
intro:
  blurbs: []
  heading: o
  description: o
main:
  heading: o
  description: o
  image1:
    alt: o
    image: /img/products-grid3.jpg
  image2:
    alt: a
    image: /img/products-grid2.jpg
  image3:
    alt: Coffee beans
    image: /img/products-grid1.jpg
testimonials:
  - author: Elisabeth Kaurismäki
    quote: >-
      The first time I tried Kaldi’s coffee, I couldn’t even believe that was
      the same thing I’ve been drinking every morning.
  - author: Philipp Trommler
    quote: >-
      Kaldi is the place to go if you want the best quality coffee. I love their
      stance on empowering farmers and transparency.
full_image: /img/Pâtes.jpg
pricing:
  heading: o
  description: o
  plans:
    - description: Perfect for the drinker who likes to enjoy 1-2 cups per day.
      items:
        - 3 lbs of coffee per month
        - Green or roasted beans"
        - One or two varieties of beans"
      plan: Small
      price: '50'
    - description: 'Great for avid drinkers, java-loving couples and bigger crowds'
      items:
        - 6 lbs of coffee per month
        - Green or roasted beans
        - Up to 4 different varieties of beans
      plan: Big
      price: '80'
    - description: Want a few tiny batches from different varieties? Try our custom plan
      items:
        - Whatever you need
        - Green or roasted beans
        - Unlimited varieties
      plan: Custom
      price: '??'
---
